/*
 - Yet an Android Simple Calculator
 - Copyright (c) 2019. Luis Miguel Soto Sánchez
 - Licensed under the Apache License, version 2.0
 -
 - See http://www.apache.org/licenses/LICENSE-2.0 for more.
 - ------
 - BeanShell Scripting for Java
 - Copyright 1997-2012 Patrick Niemeyer
 - Licensed under the Apache License, Version 2.0.
 - Granted to the Apache Software Foundation 2012
 -
 - View project LICENSE here: https://github.com/beanshell/beanshell/blob/master/LICENSE
 -
 - Note: this software comes with NO WARRANTY under any terms, for more information, please visit one of the links above.
 -------------------------------------------------------------------------------------------------*/

package io.luismiguelss.calculadora;

import android.widget.TextView;
import android.widget.Toast;

import bsh.EvalError;
import bsh.Interpreter;
import io.luismiguelss.calculadora.Styling.Formatter;

import static io.luismiguelss.calculadora.Styling.Formatter.isNumber;

public class ResultManager {

    //
    // ATTRIBUTES
    //
    private static MainActivity activity;

    //
    // CONSTRUCTOR
    //
    public ResultManager(MainActivity activity) {
        this.activity = activity;
    }

    //
    // GETTERS
    //
    public MainActivity getActivity() { return activity; }

    /**
     * Checks whether to replace the resultTextView's text with a number
     * or to append one, by checking if it's empty, or "0" in this case.
     *
     * @param value The value we want to add.
     * */
    public static void addNumber(String value) {
        TextView resultTextView = activity.findViewById(R.id.currentCalculation);

        if( (resultTextView.getText() + "").equals(activity.getString(R.string.default_text_value)))
            resultTextView.setText(value); //Replace
        else
            resultTextView.append(value);
    }

    /**
     * Checks whether to add a symbol (, * / - +) only if it is preceded by a number,
     * except (+) and (-) so they can be used by the "negativePositiveToggler()".
     *
     * @param symbol The symbol to be added. This could be any symbol.
     * */
    public void addSymbol(String symbol) {
        TextView tv = activity.findViewById(R.id.currentCalculation);

        //Check if last char is a number
        if(isNumber(tv.getText().charAt(tv.getText().length()-1))) {

            // Avoid repeating ',' in the same number. Eg.: not this -> 3,15,1 but this -> 3,15
            if(symbol.equals(activity.getString(R.string.comma))) {
                if(!activity.commaAdded) {
                    tv.append(symbol);
                    activity.commaAdded = true;
                }
            } else {
                activity.commaAdded = false;
                tv.append(symbol);
            }
        }
    }

    /**
     * Calculates the result using BeanShell.
     *
     *   It uses the string in the result TextView and interprets both numbers and symbols.
     *   BeanShell's interpreter return an object, which is then translated into a Double to
     *    display it properly.
     * */
    public void calculateResult() {
        TextView textCalculation = activity.findViewById(R.id.currentCalculation);
        TextView history = activity.findViewById(R.id.historyView);

        if(isNumber(textCalculation.getText().charAt(textCalculation.getText().length()-1))) {

            //Replace Unicode characters with Math ones
            textCalculation.setText(replaceMathSymbols(textCalculation.getText() + "",false));

            //Interpreter for the operations - BeanShell Library v2.0b4
            // Note: not secure for servers as it uses JavaScript
            Interpreter interpreter = new Interpreter();
            try {

                interpreter.eval("result = " + prepareTextForCalc(replaceMathSymbols(textCalculation.getText() + "",false)) );

                Double result = (Double)interpreter.get("result");

                //Get result
                history.setText(replaceMathSymbols(textCalculation.getText() + "=" + Formatter.df2.format(result) , true));
                textCalculation.setText(replaceMathSymbols(Formatter.df2.format(result),true));

            } catch (EvalError ee) {
                Toast.makeText(activity.getApplicationContext(), "Ha ocurrido un error en el cálculo.", Toast.LENGTH_LONG).show();

            } catch (ArithmeticException ae) {
                Toast.makeText(activity.getApplicationContext(), "No se puede dividir entre 0.", Toast.LENGTH_LONG).show();
            }
        }

    }

    /**
     * As from BeanShell, it cannot process Unicode, so to obtain results
     * this translates from Unicode to literal common strings that can be processed.
     *
     * @param text The text from where we want to replace the Unicode.
     * @param inverse Converts from common strings to Unicode.
     * @return The new string once every symbol has been replaced.
     * */
    public String replaceMathSymbols(String text, boolean inverse) {
        String result = text;

            // Unicode to String
        if (!inverse) {
            result = result.replace(activity.getString(R.string.multiply), "*");
            result = result.replace(activity.getString(R.string.divide), "/");
            result = result.replace(activity.getString(R.string.divide), "/");
            result = result.replace(activity.getString(R.string.sum), "+");
            result = result.replace(activity.getString(R.string.subtract), "-");
            result = result.replace(activity.getString(R.string.comma), ".");

            // String to Unicode
        } else {
            result = result.replace("*", activity.getString(R.string.multiply));
            result = result.replace("/", activity.getString(R.string.divide));
            result = result.replace("+", activity.getString(R.string.sum));
            result = result.replace("-", activity.getString(R.string.subtract));
            result = result.replace(".", activity.getString(R.string.comma));
        }

        return result;

    }

    /**
     * Prepares the numbers in the string to be processed by BeanShell (BSH).
     * This needs to be done because in order from BSH to return decimals, the
     * given numbers must have them too.
     *
     *      Eg.: 1+2 -> 1.0+2.0
     *      Eg.: 3+2.5 -> 3.0+2.5
     *
     * @param text The text from where converting whole numbers into decimals.
     * @return The same text but with all the whole numbers converted to decimals.
     * */
    public String prepareTextForCalc(String text) {
        String[] separatedNumbers = text.split(Formatter.REGEX_GET_NUMBERS);
        String[] separatedSymbols = text.split(Formatter.REGEX_GET_SYMBOLS,0);

        String result = "";
        int symbolCounter = 0;

        // Iterate over Numbers
        for(int i = 0; i < separatedNumbers.length; i++) {
            result += separatedNumbers[i].contains(".") ? separatedNumbers[i] : separatedNumbers[i] + ".0";

            // Iterate over Symbols
            // Note: as of regex and String.split(), some items in the array may be empty, so this
            //  only gets the symbol validating if it's empty or not.
            for (int e = symbolCounter; e < separatedSymbols.length; e++) {
                if(!separatedSymbols[e].equals("")) {
                    result += (!separatedSymbols[e].equals("")) ? separatedSymbols[e]: "";
                    symbolCounter = e+1;
                    break;
                }
            }

        }


        return result;
    }

}
