/*
 - Yet an Android Simple Calculator
 - Copyright (c) 2019. Luis Miguel Soto Sánchez
 - Licensed under the Apache License, version 2.0
 -
 - See http://www.apache.org/licenses/LICENSE-2.0 for more.
 - ------
 - BeanShell Scripting for Java
 - Copyright 1997-2012 Patrick Niemeyer
 - Licensed under the Apache License, Version 2.0.
 - Granted to the Apache Software Foundation 2012
 -
 - View project LICENSE here: https://github.com/beanshell/beanshell/blob/master/LICENSE
 -
 - Note: this software comes with NO WARRANTY under any terms, for more information, please visit one of the links above.
 -------------------------------------------------------------------------------------------------*/

package io.luismiguelss.calculadora.Styling;

import android.widget.TextView;

import java.text.DecimalFormat;

public class Formatter {

    //
    // ATTRIBUTES
    //
    public static final DecimalFormat df2 = new DecimalFormat("#.##");
    public static final String REGEX_GET_NUMBERS = "[\\+\\-\\*\\/]";
    public static final String REGEX_GET_SYMBOLS = "[0-9\\.]";

    //
    // GETTERS
    //

    /**
     * Checks whether the given character represents a number
     * between 0 and 9.
     *
     * @param character The given character to check.
     * @return true/false
     * */
    public static boolean isNumber(char character) {
        return Character.isDigit(character);
    }
}
