/*
 - Yet an Android Simple Calculator
 - Copyright (c) 2019. Luis Miguel Soto Sánchez
 - Licensed under the Apache License, version 2.0
 -
 - See http://www.apache.org/licenses/LICENSE-2.0 for more.
 - ------
 - BeanShell Scripting for Java
 - Copyright 1997-2012 Patrick Niemeyer
 - Licensed under the Apache License, Version 2.0.
 - Granted to the Apache Software Foundation 2012
 -
 - View project LICENSE here: https://github.com/beanshell/beanshell/blob/master/LICENSE
 -
 - Note: this software comes with NO WARRANTY under any terms, for more information, please visit one of the links above.
 -------------------------------------------------------------------------------------------------*/

package io.luismiguelss.calculadora.Listeners;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import io.luismiguelss.calculadora.Styling.Formatter;
import io.luismiguelss.calculadora.MainActivity;
import io.luismiguelss.calculadora.R;
import io.luismiguelss.calculadora.ResultManager;

public class MiscellaneousListener implements View.OnClickListener, View.OnLongClickListener {

    ResultManager manager;

    //
    // CONSTRUCTOR
    //
    public MiscellaneousListener(MainActivity activity) {
        manager = new ResultManager(activity);
    }

    @Override
    public void onClick(View v) {
        TextView resultTV = manager.getActivity().findViewById(R.id.currentCalculation);
        TextView history = manager.getActivity().findViewById(R.id.historyView);
        String resultText = resultTV.getText() + "";

        switch (v.getId()) {

            //Clear All Screen
            case R.id.btn_ClearHistoryResult:
                resultTV.setText(manager.getActivity().getString(R.string.default_text_value));
                history.setText(manager.getActivity().getString(R.string.default_text_value));
                break;

            //Clear Actions
            case R.id.btn_clearAll:
                resultTV.setText(manager.getActivity().getString(R.string.default_text_value));
                break;

            case R.id.btn_deleteLast:
                if(!resultTV.equals(manager.getActivity().getString(R.string.default_text_value))) {
                    if(resultText.length() > 1)
                        resultTV.setText(resultText.substring(0, resultText.length() - 1));
                    else
                        resultTV.setText(manager.getActivity().getString(R.string.default_text_value));
                }
                break;

            // CALCULATE
            case R.id.btn_result:
                manager.calculateResult();
                break;
            default: break;
        }
    }

    @Override
    public boolean onLongClick(View v) {

        //Only work if it's the historyView
        switch (v.getId()) {
            case R.id.historyView:
                ( (TextView) manager.getActivity().findViewById(R.id.historyView)).setText(
                                            manager.getActivity().getString(
                                                    R.string.default_text_value
                                            )
                );

                Toast.makeText(manager.getActivity(), "History Cleared", Toast.LENGTH_SHORT).show();
                break;

            default: break;
        }


        return true;
    }
}
