/*
 - Yet an Android Simple Calculator
 - Copyright (c) 2019. Luis Miguel Soto Sánchez
 - Licensed under the Apache License, version 2.0
 -
 - See http://www.apache.org/licenses/LICENSE-2.0 for more.
 - ------
 - BeanShell Scripting for Java
 - Copyright 1997-2012 Patrick Niemeyer
 - Licensed under the Apache License, Version 2.0.
 - Granted to the Apache Software Foundation 2012
 -
 - View project LICENSE here: https://github.com/beanshell/beanshell/blob/master/LICENSE
 -
 - Note: this software comes with NO WARRANTY under any terms, for more information, please visit one of the links above.
 -------------------------------------------------------------------------------------------------*/

package io.luismiguelss.calculadora.Listeners;

import android.view.View;

import io.luismiguelss.calculadora.MainActivity;
import io.luismiguelss.calculadora.R;
import io.luismiguelss.calculadora.ResultManager;

public class SymbolListener implements View.OnClickListener {

    ResultManager result;

    //
    // CONSTRUCTOR
    //
    public SymbolListener(MainActivity activity) {
        result = new ResultManager(activity);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            //Comma
            case R.id.btn_decimal:
                result.addSymbol(result.getActivity().getString(R.string.comma));
                break;

            // Math
            case R.id.btn_multiply:
                result.addSymbol(result.getActivity().getString(R.string.multiply));
                break;

            case R.id.btn_divide:
                result.addSymbol(result.getActivity().getString(R.string.divide));
                break;

            case R.id.btn_sum:
                result.addSymbol(result.getActivity().getString(R.string.sum));
                break;

            case R.id.btn_subtract:
                result.addSymbol(result.getActivity().getString(R.string.subtract));
                break;

            default: break;
        }

    }
}
