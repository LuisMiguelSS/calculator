/*
 - Yet an Android Simple Calculator
 - Copyright (c) 2019. Luis Miguel Soto Sánchez
 - Licensed under the Apache License, version 2.0
 -
 - See http://www.apache.org/licenses/LICENSE-2.0 for more.
 - ------
 - BeanShell Scripting for Java
 - Copyright 1997-2012 Patrick Niemeyer
 - Licensed under the Apache License, Version 2.0.
 - Granted to the Apache Software Foundation 2012
 -
 - View project LICENSE here: https://github.com/beanshell/beanshell/blob/master/LICENSE
 -
 - Note: this software comes with NO WARRANTY under any terms, for more information, please visit one of the links above.
 -------------------------------------------------------------------------------------------------*/

package io.luismiguelss.calculadora.Listeners;

import android.view.View;

import io.luismiguelss.calculadora.MainActivity;
import io.luismiguelss.calculadora.R;
import io.luismiguelss.calculadora.ResultManager;

public class NumberListener implements View.OnClickListener {

    ResultManager result;

    //
    // CONSTRUCTOR
    //
    public NumberListener(MainActivity activity) {
        result = new ResultManager(activity);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_number0: result.addNumber("0");
                break;

            case R.id.btn_number1: result.addNumber("1");
                break;

            case R.id.btn_number2: result.addNumber("2");
                break;

            case R.id.btn_number3: result.addNumber("3");
                break;

            case R.id.btn_number4: result.addNumber("4");
                break;

            case R.id.btn_number5: result.addNumber("5");
                break;

            case R.id.btn_number6: result.addNumber("6");
                break;

            case R.id.btn_number7: result.addNumber("7");
                break;

            case R.id.btn_number8: result.addNumber("8");
                break;

            case R.id.btn_number9: result.addNumber("9");
                break;

            default: break;
        }

    }
}
