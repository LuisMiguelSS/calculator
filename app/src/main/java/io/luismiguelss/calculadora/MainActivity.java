/*
 - Yet an Android Simple Calculator
 - Copyright (c) 2019. Luis Miguel Soto Sánchez
 - Licensed under the Apache License, version 2.0
 -
 - See http://www.apache.org/licenses/LICENSE-2.0 for more.
 -
 - BeanShell Scripting for Java
 - Copyright 1997-2012 Patrick Niemeyer
 - Licensed under the Apache License, Version 2.0.
 - Granted to the Apache Software Foundation 2012
 -
 - View project LICENSE here: https://github.com/beanshell/beanshell/blob/master/LICENSE
 -
 - Note: this software comes with NO WARRANTY under any terms, for more information, please visit one of the links above.
 -----------------------------------------------------------------------------*/

package io.luismiguelss.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import io.luismiguelss.calculadora.Listeners.MiscellaneousListener;
import io.luismiguelss.calculadora.Listeners.NumberListener;
import io.luismiguelss.calculadora.Listeners.SymbolListener;


public class MainActivity extends AppCompatActivity {

    // Formatter
    public boolean commaAdded = false;
    public ResultManager manager;

    // Texts
    TextView history;
    TextView currentResult;

    // Functionality buttons
    Button btnclearAll;
    Button btndeleteLast;

    Button btnMultiply;
    Button btnDivide;
    Button btnSum;
    Button btnSubtract;
    Button btnResult;

    Button btnDecimal;
    Button btnClearHistoryResult;

    // Number buttons
    Button btnNumber0;
    Button btnNumber1;
    Button btnNumber2;
    Button btnNumber3;
    Button btnNumber4;
    Button btnNumber5;
    Button btnNumber6;
    Button btnNumber7;
    Button btnNumber8;
    Button btnNumber9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Controller for all calculations
        manager = new ResultManager(this);

        // Hide Action Bar
        try {
            getSupportActionBar().hide();

        } catch (NullPointerException npe) {
            Toast.makeText(getApplicationContext(),"Error al intentar ocultar la barra de la aplicación.",Toast.LENGTH_LONG);
        }

        //
        // Setting Views
        //
        history = findViewById(R.id.historyView);
        history.setMovementMethod(new ScrollingMovementMethod());
        currentResult = findViewById(R.id.currentCalculation);

        btnclearAll = findViewById(R.id.btn_clearAll);
        btndeleteLast = findViewById(R.id.btn_deleteLast);

        btnMultiply = findViewById(R.id.btn_multiply);
        btnDivide = findViewById(R.id.btn_divide);
        btnSum = findViewById(R.id.btn_sum);
        btnSubtract =  findViewById(R.id.btn_subtract);
        btnResult = findViewById(R.id.btn_result);

        btnDecimal = findViewById(R.id.btn_decimal);
        btnClearHistoryResult = findViewById(R.id.btn_ClearHistoryResult);

        btnNumber0 = findViewById(R.id.btn_number0);
        btnNumber1 = findViewById(R.id.btn_number1);
        btnNumber2 = findViewById(R.id.btn_number2);
        btnNumber3 = findViewById(R.id.btn_number3);
        btnNumber4 = findViewById(R.id.btn_number4);
        btnNumber5 = findViewById(R.id.btn_number5);
        btnNumber6 = findViewById(R.id.btn_number6);
        btnNumber7 = findViewById(R.id.btn_number7);
        btnNumber8 = findViewById(R.id.btn_number8);
        btnNumber9 = findViewById(R.id.btn_number9);


        //
        // OnClickListeners
        //
        history.setOnLongClickListener(new MiscellaneousListener(this));
        btnclearAll.setOnClickListener(new MiscellaneousListener(this));
        btndeleteLast.setOnClickListener(new MiscellaneousListener(this));

        btnDecimal.setOnClickListener(new SymbolListener(this));
        btnClearHistoryResult.setOnClickListener(new MiscellaneousListener(this));

        btnMultiply.setOnClickListener(new SymbolListener(this));
        btnDivide.setOnClickListener(new SymbolListener(this));
        btnSum.setOnClickListener(new SymbolListener(this));
        btnSubtract.setOnClickListener(new SymbolListener(this));
        btnResult.setOnClickListener(new MiscellaneousListener(this));

        btnNumber0.setOnClickListener(new NumberListener(this));
        btnNumber1.setOnClickListener(new NumberListener(this));
        btnNumber2.setOnClickListener(new NumberListener(this));
        btnNumber3.setOnClickListener(new NumberListener(this));
        btnNumber4.setOnClickListener(new NumberListener(this));
        btnNumber5.setOnClickListener(new NumberListener(this));
        btnNumber6.setOnClickListener(new NumberListener(this));
        btnNumber7.setOnClickListener(new NumberListener(this));
        btnNumber8.setOnClickListener(new NumberListener(this));
        btnNumber9.setOnClickListener(new NumberListener(this));

    }

}
